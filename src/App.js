import React, { Component, Fragment } from 'react';
import { Switch, Route, Link } from 'react-router-dom'
import EventEmitter from "./events"
import $ from "jquery"


const ee = new EventEmitter();


class App extends Component {

	constructor(props) {
		super(props);
		this.setUser = this.setUser.bind(this);
		this.state = {
			isAuthorized: false,
			user: {
				id: 0,
				username: "Guest",
				avatar: undefined,
			}
		};
	}

	componentDidMount() {
		console.log("App componentDidMount");
		ee.on("user", this.setUser);
	}

	componentWillUnmount() {
		console.log("App componentWillUnmount");
		ee.removeListener("user", this.setUser)
	}

	setUser(user) {
		console.log("setUser", user);
		this.setState({user})
	}

    render() {
        return (
            <Fragment>
                <Header {...this.state} />
				<SvgIcons />
                <Main {...this.state} />
				<Footer />
            </Fragment>

        );
    }
}


class Header extends Component {

	constructor(props) {
		super(props);
	}


	componentDidMount() {
		console.log("Header componentDidMount");
	}

	componentWillUnmount() {
		console.log("Header componentWillUnmount");
	}

	render() {

		console.log("Header", this.props)

		return (
			<div id="top">
				<div id="logo" className="catamaran">PolyAlpha</div>
				<div id="menu" className="catamaran">
					<a className="selected animate" href="#">Messenger POC</a>
					<a className="animate" href="#">The DAICO</a>
					<a className="animate" href="#">Give Feedback</a>
				</div>
				{this.props.user.id && (
					<div id="user-bar">
					<span className="name catamaran animate">
						{this.props.user.username} <Svg id="svg-select" className="icon" />
					</span>
						<a className="avatar" href="#"><img src={this.props.user.avatar} /></a>
					</div>
				)}
			</div>
		);
	}
}


class Main extends Component {

	constructor(props) {
		super(props);
		this.state = {...this.state}
	}

	componentWillMount() {
		console.log("Main componentDidMount");
	}

	componentWillUnmount() {
		console.log("Main componentWillUnmount");
	}

	render() {
		return (
			<div id="main">
				<div id="main-block">
					<MainTitle title={this.state.title} />
					<Switch>
						<Route exact path='/' component={withProps(Home, {...this.state})} />
						<Route exact path='/signup' component={withProps(Signup, {...this.state})} />
						<Route exact path='/chat' component={withProps(Chat, {...this.state})} />
					</Switch>
				</div>
			</div>
		);
	}
}


class MainTitle extends Component {

	constructor(props) {
		super(props);
		this.setTitle = this.setTitle.bind(this);
		this.state = {
			title: this.props.title || "Hello!"
		}
	}

	componentWillMount() {
		console.log("MainTitle componentDidMount");
		ee.on("title", this.setTitle)
	}

	componentWillUnmount() {
		console.log("MainTitle componentWillUnmount");
		ee.removeListener("title", this.setTitle)
	}

	setTitle(title) {
		if (this.state.title === title) return;
		this.setState({title});
		document.getElementsByTagName("title")[0].innerHTML = title;
	}

	render() {
		return <h1 className="main-title raleway">{this.state.title}</h1>;
	}
}


class Home extends Component {

	constructor(props) {
		super(props);
		console.log(this.props)
	}

	componentDidMount() {
		console.log("Home componentDidMount");
	}

	render() {
		return this.props.isAuthorized ? <Chat {...this.props} /> : <Hello {...this.props} />;
	}
}


class Hello extends Component {

	title = "Hello";

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		console.log("Hello componentDidMount");
		ee.emit("title", this.title)
	}

	render() {
		let signinButtonText = "I want my bids, log me in",
			pp = ["Welcome to the next generation decentralised, private and scaleable instant messenger that pays you ABT tokens for your attention.", "When you sign up you will recieve a PolyAlpha messenger address pair on the Ethereum Testnet. If you already have an account, log in with your private key."];
		return (
			<div id="body-index">
				<div className="info catamaran">{pp.map(x=><p>{x}</p>)}</div>
				<div className="buttons-block">
					<div className="signup">
						<div className="block">
							<div className="row">
								<SignupButton />
							</div>
						</div>
					</div>
					<div className="signin">
						<div className="block">
							<div className="row">
								<div className="button catamaran" onClick={this.signinHandler}>
									<Svg id="svg-crown" className="icon" />{signinButtonText}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}


class SignupButton extends  Component {

	constructor(props) {
		super(props);
		this.handler = this.handler.bind(this);
		this.state = {...this.props};
		console.log("SignupButton", this.state)
	}

	handler(e) {
		if (!this.state.isSignup) return;
		console.log("signupHandler", e.target);
		if (this.state.isOk) {
			this.setState({add:true},()=>{
				ee.emit("signup-state", this.state)
			})
		}

	}

	componentWillReceiveProps(nextProps) {
		console.log("componentWillReceiveProps", nextProps)
		this.setState(nextProps);
	}

	render() {
		console.log("SignupButton render", this.state)
		let text = "I’m new, create an address pair for me";

		return this.state.isSignup ? (

			<div className="row empty">
				<div className="button catamaran" onClick={this.handler}>
					<Svg id="svg-lightning" className="icon" />{text}
				</div>
			</div>

		) : (

			<div className="row">
				<Link to={!this.state.isSignup ? "/signup" : "/chat"} className="button catamaran">
					<Svg id="svg-lightning" className="icon" />
					{text}
				</Link>
			</div>
		)
	}
}


class SignupForm extends  Component {

	constructor(props) {
		super(props);
		this.state = {...props};
		this.formHandler = this.formHandler.bind(this);
		this.avatarsPath = "/i/avatars";
		this.avatars = ["sandy.png","sandy.png","sandy.png","sandy.png","sandy.png","sandy.png","sandy.png",
			"sandy.png","sandy.png"]
	}

	componentDidMount() {

	}

	checkName(name) {
		return /\w{5,}/.test(name)
	}

	formHandler(e) {
		let $el = $(e.target);
		window.E = $el
		console.log(e.target.name, e.target.value)
		if (e.target.name === "username") {
			let x = e.target.value.trim();
			if(!this.checkName(x)) x = undefined;
			this.setUsername(x)
		} else {
			let x = !$el.hasClass("avatar") ? $el.parent() : $el;
			let src = undefined;
			if (!x.hasClass("selected")) {
				x.parent().find(".selected").removeClass("selected");
				x.addClass("selected");
				src = x.find("img").attr("src")
			} else {
				x.removeClass("selected")
			}

			this.setAvatar(src)
		}

	}

	setUsername(name) {
		if (this.state.username !== name) {
			this.setState({username: name}, ()=>{
				console.log(this.state);
				this.checkOk()
			});
		} else {
			this.checkOk()
		}
	}

	setAvatar(src) {
		console.log("setAvatar", src);
		if (this.state.avatar !== src) {
			this.setState({avatar: src}, ()=>{
				console.log(this.state)
				this.checkOk()
			});
		} else {
			this.checkOk()
		}
	}

	checkOk() {
		let ok = this.state.avatar && this.state.username;
		if (ok !== this.state.isOk) {
			this.setState({isOk:!!ok}, () => {
				ee.emit("signup-state", this.state)
			})
		} else {
			ee.emit("signup-state", this.state)
		}
	}

	render() {
		return (
			<Fragment>
				<div className={"row"+(!this.state.username ? " empty" : "")}>
					<div className="form-row">
						<label>Username</label>
						<input name="username" className="input" type="text" placeholder="Type in a username you want to create" onChange={this.formHandler} />
					</div>
				</div>
				<div className="row">
					<label>Avatar</label>
					<div className="placeholder">Select an avatar</div>
					<div className="select-avatar">
						{this.avatars.map((x, i)=>{
							return (
								<div key={i} className="avatar animate" onClick={this.formHandler}>
									<img src={this.avatarsPath+"/"+x}/>
								</div>
							)
						})}
						<div className="avatar animate upload" onClick={this.formHandler}>
							<input name="avatar" type="file" onChange={this.formHandler}/>
							<div className="text animate">Upload your own</div>
						</div>
					</div>
				</div>
			</Fragment>
		);
	}
}


class Signup extends  Component {

	title = "Signup";

	constructor(props) {
		super(props);
		this.state = {
			isOk: false,
			username: undefined,
			avatar: undefined
		};
		this.setOk = this.setOk.bind(this)
	}

	componentDidMount() {
		ee.emit("title", this.title);
		ee.on("signup-state", this.setOk)

	}

	componentWillUnmount() {
		ee.removeListener("signup-state", this.setOk)
	}

	isOk() {
		return this.state.isOk.username && this.state.isOk.avatar
	}

	setOk(state) {
		console.log("setOk", state)
		this.setState(state, ()=>{
			if (this.state.add) this.addUser();
		})
	}

	addUser() {
		console.log("ADD USER", this.state)
		ee.emit("user", {
			id:42,
			username: this.state.username,
			avatar: this.state.avatar
		})
	}

	render() {
		return (
			<div id="body-index">
				{this.state.isOk.username}
				<div className="buttons-block">
					<div className="signup">
						<div className="block">
							<SignupForm {...this.state} />
							<SignupButton isOk={this.state.isOk} isSignup={true} />
						</div>
					</div>
				</div>
			</div>
		);
	}
}


class Chat extends Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		console.log("Chat Hello componentDidMount");
	}

	render() {
		return (
			<Link to='/'>Home</Link>
		);
	}
}


class Footer extends Component {
	render() {
		let text = "If you experience any bugs or require help please send a message to “PolyAlpha.io Assistant”.";
		return (
			<Fragment>
				<div id="logo-bg"/>
				<div id="gradient"/>
				<div id="footer">
					<div className="text catamaran">{text}</div>
				</div>
			</Fragment>
		);
	}
}


function SvgIcons() {
	return (
		<svg width="0" height="0" className="hide">
			<defs>
				<path id="svg-back" fillRule="evenodd" clipRule="evenodd"
					  d="M8 1.39746L6.76953 0L0 7.68579L0.277344 8L0 8.31421L6.76953 16L8 14.6025L2.18555 8L8 1.39746Z"
					  strokeWidth="0"/>
				<path id="svg-crown"
					  d="M27 0L20.25 6.66667L13.5 0L6.75 6.66667L0 0V18.3333C0 19.2538 0.75552 20 1.6875 20H25.3125C26.2445 20 27 19.2538 27 18.3333V0Z"
					  strokeWidth="0"/>
				<path id="svg-lightning" d="M7.87037 0L0 14.0741H6.85185L5.55556 25L13.4259 10H6.85185L7.87037 0Z"
					  transform="translate(1 1)" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
				<path id="svg-select"
					  d="M6 4.31137L1.72153 0.278417C1.3277 -0.0928057 0.68919 -0.0928057 0.295367 0.278417C-0.0984557 0.64964 -0.0984557 1.25151 0.295367 1.62273L6 7L11.7046 1.62273C12.0985 1.25151 12.0985 0.64964 11.7046 0.278417C11.3108 -0.0928057 10.6723 -0.0928057 10.2785 0.278417L6 4.31137Z"
					  strokeWidth="0"/>
			</defs>
		</svg>
	)
}


function withProps(WrapperComponent, props) {
	return class extends Component {
		render() {
			return <WrapperComponent {...props} />
		}
	}
}


const Svg = (props) => {
	let {id, className, ...params} = props;
	let cc = `svg ${id} ${(className || "")}`.trim();
	return (
		<svg className={cc} {...params}>
			<use xlinkHref={"#"+id} />
		</svg>
	)
};


export default App;
